import sqlite3
from sqlite3 import Error


def create_connection(db_file):
    """
    Create database connection to the SQLLite database, specifically by the db_file.
    :param db_file:
    :return: connection object or none
    """
    conn = None
    try:
        conn = sqlite3.connect(db_file)
    except ImportError as error:
        print(error)
    return conn


def select_all_rows(conn):
    """
    Query all rows in the task table
    :param conn:
    :return: row
    """
    cur = conn.cursor()
    cur.execute("SELECT * FROM tasks")
    rows = cur.fetchall()
    sources = []
    for row in rows:
        sources.append(rows)
    print(sources)
    return sources


def create_table(conn, create_table_sql):
    """ create a table from the create_table_sql statement
    :param conn: Connection object
    :param create_table_sql: a CREATE TABLE statement
    :return:
    """
    try:
        c = conn.cursor()
        c.execute(create_table_sql)
    except Error as e:
        print(e)


# if __name__ == '__main__':

conn = sqlite3.connect("pythonsqlite.db")

sql_create_link_table = """ CREATE TABLE IF NOT EXISTS projects (
                                    rtsp-address text NOT NULL
                                ); """
# create a database connection
if conn is not None:
    # create projects table
    create_table(conn, sql_create_link_table)
else:
    print("Error! cannot create the database connection.")
